# OLC1 - Conferencia "Creando un IDE con TypeScript"

Pablo Andrés Hernández Rivera

## Introducción

El siguiente proyecto es una aplicación web creada en el framework Angular, el cual es nativo para el lenguaje TypeScript. La misma tiene integrado el código para realizar un análisis de un lenguaje definido con fines ilustrativos para la implementación del patrón estrategia en la ejecución de instrucciones interpretadas. Esto se hace integrando módulos de JavaScript generados por el generador de analizadores léxicos Jison, con un servicio de Angular.

## Requerimientos

Para instalar esta aplicación es necesario tener instalado Node.js y NPM. Node es un intérprete de JavaScript que permite la creación de aplicaciones altamente escalables. NPM es un manejador de paquetes para Node.js. Es necesario instalar además Angular-CLI y Jison de forma global. Esta información puede encontrarse en la documentación oficial de cada herramienta:

[Node.js](https://nodejs.org/es/)
[NPM](https://www.npmjs.com/)
[Angular](https://angular.io/)
[Jison](https://zaa.ch/jison/)
[Angular Material UI](https://material.angular.io/)
[Ace](https://ace.c9.io/)

## Sobre la aplicación

La aplicación está hecha con Angular-CLI y utiliza los componentes de diseño Angular Material, para darle una apariencia intuitiva y moderna. Para poder utilizar esta aplicación, es necesario descargar el contenido de este repositorio y ejecutar el siguiente comando en la ruta donde se ha almacenado:

```
# Se encarga de instalar las dependencias de la aplicación.
$ npm install
# Se encarga de correr el proyecto. La primera vez que se ejecuta puede tomar mucho tiempo.
$ ng serve
```

En el navegador, es necesario entrar a la ruta "localhost:4200" para visualizar la aplicación.

Angular provee un servidor para pruebas de desarrollo, en el cual los cambios en el código se ven reflejados de forma instantánea. Sin embargo, este servidor no es óptimo para un ambiente de producción. Es posible compilar aplicaciones de Angular y generar páginas estáticas.

## Sobre el intérprete

El intérprete fue creado con el generador de analizadores Jison. Las expresiones regulares están definidas al inicio del archivo, mientras que la gramática y definiciones dirigidas por la sintaxis se encuentran posteriormente. En términos generales, se han creado APIs en JavaScript para poder integrar instrucciones en el proceso de análisis sintáctico, las cuales definen estructuras (objetos) que representan instrucciones en el lenguaje (descrito a continuación). El resultado de resultado del análisis de Jison será, en consecuencia, un AST (árbol de sintaxis abstracta).

Para generar un analizador con Jison es necesario crear un archivo con las definiciones y la extensión ".jison". El archivo de este proyecto se encuentra en la ruta [/src/app/analizador/parser/gramatica.jison](/src/app/analizador/parser/gramatica.jison). Es necesario abrir una terminal en esta carpeta y correr el siguiente comando para generar el analizador:

```
# Genera el analizador del lenguaje. 
$ jison gramatica.jison
```

Existen diferentes opciones adicionales que pueden ser agregadas para que el analizador generado sea integrado de forma segura con la apliación, pues por defecto, el analizador es en sí una aplicación.

### Advertencia para integración con Angular

Dado que el analizador generado es una aplicación en sí, debe modificarse el archivo resultante para poderlo integrar de forma segura con la aplicación de Angular. Esto porque Jison utiliza la API "fs" en la lectura de archivos, y Angular como aplicación web lo bloquea por defecto. En los analizadores generados por Jison, se encuentra al final un fragmento de código como el siguiente:

```
//Gramatica.js
var source = require('fs').readFileSync(require('path').normalize(args[1]), "utf8");
return exports.parser.parse(source);
```

Es necesario eliminar estas dos líneas del archivo para que funcione con Angular, o bien, modificar la configuración de la aplicación para que permita la API "fs".

## Sobre el lenguaje

El lenguaje es capaz de reconocer una serie de instrucciones separadas por punto y coma (;). Las intrucciones permitidas son las siguientes:

1. Declaración de variables: Es posible declarar variables, las cuales constan de un identificador y un valor. El valor está dado por cualquier expresión aritmética de suma, resta, multiplicación, división, residuo y negación, que incluya números u otros identificadores previamente especificados.

2. Función "print": Esta función permite mostrar en consola, el valor de una expresión. 

### Ejemplo

Entrada:

```
id = 5;
print(id);
id2 = id + 6;
print(id2 / 7 - - 6);
```

Salida:

```
5 7.571428571428571 
```

## Conclusión y recomendaciones

Existen muchas oportunidades de mejora en esta aplicación, como la validación de errores, de tipos, semántica y optimización de funciones. El fin es únicamente demostrativo y no debería usarse en un ambiente de producción. Se espera sea de utilidad para estudiantes, así como provea un punto de partida para la construcción de intérpretes basados en el lenguaje TypeScript.