import { Component, Input, OnInit, ViewChild } from '@angular/core';

import { IdeComponent } from '../ide/ide.component';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit
{

  private _parent: IdeComponent;

  @Input() set parent(value: IdeComponent)
  {
    this._parent = value;
    this._parent.editors.push(this);
  }

  constructor() { }

  ngOnInit(): void
  {
  }

  @ViewChild('editor') editor;
  text: string = "";

  ngAfterViewInit()
  {

    this.editor.setMode("javascript");
    this.editor.setTheme("eclipse");

    this.editor.getEditor().setOptions({
      enableBasicAutocompletion: true,
      maxLines: 16,
      minLines: 16,
      fontSize: '18pt'
    });

  }

  public getContent()
  {
    return this.text;
  }

}
