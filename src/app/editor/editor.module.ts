import { AceEditorModule } from 'ng2-ace-editor';
import { CommonModule } from '@angular/common';
import { EditorComponent } from './editor.component'
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [EditorComponent],
  imports: [
    CommonModule,
    AceEditorModule
  ],
  exports: [
    EditorComponent
  ]
})
export class EditorModule { }
