# Intérprete

Es necesario comprender la sinergia de los archivos contenidos en este folder para tener una mejor idea del funcionamiento total de la aplicación, por lo que se da un resumen de cada una de las carpetas contenidas en este módulo.

## AST

Esta carpeta contiene el conjunto de clases abstractas que facilitan el comportamiento de las clases que realizan la ejecución de las instrucciones.

## Elementos

En el lenguaje definido, son las clases que representan la abstracción de los símbolos y valores posibles.
- Un primitivo es un valor explícito, entiéndase, un número, caracter, decimal, etc.
- Un símbolo es una variable almacenada en un ámbito, la cual es nombrada con un identificador, y que referencia a un valor.
- El uso de enumerativos permite la comparación directa entre distintos símbolos.
- Las funciones, tanto nativas como definidas por el usuario, son consideradas símbolos cuyo valor es determinado por una serie de instrucciones.

## Instrucciones

En el lenguaje definido, las instrucciones son sentencias que realizan acciones. El programa al ser interpretado ejecuta una serie de instrucciones que transforma entradas en salidas. Programas y lenguajes más complejos podrían necesitar abstraerse a instrucciones y expresiones.

- Escribir una variable o acceder a un identificador podría considerarse una sentencia de acceso a memoria.
- Las operaciones aritméticas son expresiones.

## parser

Conjunto de funciones en JavaScript que realizan el proceso de análisis léxico y sintáctico.

## scope

"Scope" es la palabra en inglés para ámbito o entorno. Este agrupa clases que son globales a la ejecución de instrucciones. Es necesario saber que el proceso de análisis léxico y sintáctico genera un árbol de sintaxis abstracta, el cual es responsable de realizar las validaciones semánticas en la ejecución, compilación o traducción de código fuente.