%lex

%options case-insensitive

%s                comment

%%


"//".*								/**/
"/*"                this.begin('comment');
<comment>"*/"       this.popState();
<comment>.							/**/

\s+									/**/

"("                 return 'para';
")"                 return 'parc';
";"                 return 'puntocoma';

"print"				return 'resprint';

"="                 return 'igual';
"+"                 return 'suma';
"-"                 return 'resta';
"*"                 return 'multiplicacion';
"/"                 return 'division';
"%"                 return 'residuo';

[0-9]+("."[0-9]+)		    return 'numdecimal';
[0-9]+           		    return 'numentero';

[a-zA-Z_][a-zA-Z0-9_]*   	return 'id';

<<EOF>>                 	return 'EOF';

.							{ ast.errores.push( `Lexema no reconocido: "${yytext}", linea: ${yylloc.first_line}, columna: ${yylloc.first_column}`) }

/lex

%start ini

%{

	const TIPO = require("./constructor").TIPO;
	const TIPO_OPERACION = require("./constructor").TIPO_OPERACION;
	const api = require("./api").api;

	let ast;

%}

%right tk_asignacion

%%

ini
	: start instrucciones EOF { return ast; }
;

start
	: { ast = api.getAST(); }
;

instrucciones
	: instrucciones instruccion								{ ast.instrucciones.push($2); }
	| instruccion											{ ast.instrucciones.push($1); }
	| err                                                  	{ ast.errores.push(`No se esperaba este componente: ${yytext}, linea: ${this._$.first_line}, columna: ${this._$.first_column}`); }
;

instruccion
	: print
	| declaracion
;

print
	: resprint para E parc puntocoma						{ $$ = api.getPrint($3, this._$.first_line, this._$.first_column); }
;

declaracion
	: id igual E puntocoma									{ $$ = api.getDeclaracion($1, $3, this._$.first_line, this._$.first_column); }
;

E
	: E suma F												{ $$ = api.getOperacionBinaria(TIPO_OPERACION.suma, $1, $3, this._$.first_line, this._$.first_column); }
	| E resta F												{ $$ = api.getOperacionBinaria(TIPO_OPERACION.resta, $1, $3, this._$.first_line, this._$.first_column); }
	| F														{ $$ = $1; }
;

F
	: F multiplicacion H									{ $$ = api.getOperacionBinaria(TIPO_OPERACION.multiplicacion, $1, $3, this._$.first_line, this._$.first_column); }
	| F division H											{ $$ = api.getOperacionBinaria(TIPO_OPERACION.division, $1, $3, this._$.first_line, this._$.first_column); }
	| F residuo H											{ $$ = api.getOperacionBinaria(TIPO_OPERACION.residuo, $1, $3, this._$.first_line, this._$.first_column); }
	| H														{ $$ = $1; }
;

H
	: resta H												{ $$ = api.getOperacionUnaria(TIPO_OPERACION.menos, $2, this._$.first_line, this._$.first_column); }
	| L														{ $$ = $1; }
;

L
	: numentero												{ $$ = api.getPrimitivo(TIPO.entero, $1, this._$.first_line, this._$.first_column); }
	| numdecimal											{ $$ = api.getPrimitivo(TIPO.decimal, $1, this._$.first_line, this._$.first_column); }
	| id													{ $$ = api.getAccesoID($1, this._$.first_line, this._$.first_column); }
;
