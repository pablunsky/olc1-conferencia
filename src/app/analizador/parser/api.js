

const api = {

  getAST: function ()
  {

    return { instrucciones: [], errores: [] };

  },

  getOperacionBinaria: function (tipo, izquierda, derecha, ln, col)
  {

    return { id: "OperacionBinaria", tipo: tipo, izq: izquierda, der: derecha, ln: ln, col: col };

  },

  getOperacionUnaria: function (tipo, unario, ln, col)
  {

    return { id: "OperacionUnaria", tipo: tipo, unario: unario, ln: ln, col: col };

  },

  getPrint: function (expresion, ln, col)
  {

    return { id: "Print", valor: expresion, ln: ln, col: col };

  },

  getPrimitivo: function (tipo, valor, ln, col)
  {

    return { id: "Primitivo", tipo: tipo, valor: valor, ln: ln, col: col };

  },

  getDeclaracion: function (id, valor, ln, col)
  {
    return { id: "Declaracion", identificador: id, valor: valor, ln: ln, col: col }
  },

  getAccesoID: function (id, ln, col)
  {
    return { id: "AccesoID", valor: id, ln: ln, col: col }
  }

}

module.exports.api = api;
