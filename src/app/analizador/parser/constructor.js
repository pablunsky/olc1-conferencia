const TIPO_OPERACION = {

  suma: 'suma',
  resta: 'resta',
  multiplicacion: 'multiplicacion',
  division: 'division',
  residuo: 'residuo',
  menos: 'menos'

}

const TIPO = {

  entero: 'entero',
  decimal: 'decimal'

}

module.exports.TIPO = TIPO;
module.exports.TIPO_OPERACION = TIPO_OPERACION;