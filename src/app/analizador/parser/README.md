# Aclaraciones sobre el analizador

Si bien, Jison genera un analizador, para poderlo integrar con TypeScript es necesario extraer la función de análisis en un módulo que pueda importar el servicio de Angular. Para esto, el archivo "parserAPI" provee un objeto con la función del parser generado por Jison, en las líneas:

```
let parser = require('./gramatica');
let ast;

try
{

    ast = parser.parse(entrada);

} catch (e)
{
    ast = { errores: [`Error irrecuperable: ${e}`] };
}

return ast;
```

Otros archivos, como "constructor.js" y "api.js" son un conjunto de funciones las cuales proveen estructuras que pueden ser analizadas por la clase "Arbol.ts" en la creación de instancias para aprovechar las ventajas del tipado en TypeScript.