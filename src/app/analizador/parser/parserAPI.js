

const parserAPI = {

  parse: function (entrada)
  {

    let parser = require('./gramatica');
    let ast;

    try
    {

      ast = parser.parse(entrada);

    } catch (e)
    {
      ast = { errores: [`Error irrecuperable: ${e}`], instrucciones: [] };
    }

    return ast;

  }
}

module.exports.parserAPI = parserAPI;
