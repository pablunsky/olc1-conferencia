import { Arbol } from '../scope/Arbol';
import { Simbolo } from '../Elementos/Simbolo';

export abstract class Expresion
{
    loc: string;

    constructor(fila: number, columna: number)
    {
        this.loc = `fila: ${fila}, columna: ${columna}`;
    }

    abstract ejecutar(s: Map<string, Simbolo>, a: Arbol): any;
}
