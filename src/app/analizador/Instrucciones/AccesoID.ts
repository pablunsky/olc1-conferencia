import { Arbol } from '../scope/Arbol';
import { Expresion } from '../AST/AST';
import { Simbolo } from '../Elementos/Simbolo';

export class AccesoID extends Expresion
{
    ejecutar(s: Map<string, Simbolo>, a: Arbol)
    {
        return s.get(this.id).valor;
    }

    id: string;

    constructor(id: string, fila: number, columna: number)
    {
        super(fila, columna);
        this.id = id;
    }
}