import { Arbol } from '../scope/Arbol';
import { Expresion } from '../AST/AST';
import { Simbolo } from '../Elementos/Simbolo';
import { TipoOperacion } from '../Elementos/TipoOperacion';

export class OperacionBinaria extends Expresion
{
    ejecutar(s: Map<string, Simbolo>, a: Arbol)
    {
        let izq = this.izq.ejecutar(s, a);
        let der = this.der.ejecutar(s, a);

        switch (this.tipo)
        {
            case TipoOperacion.suma:
                return izq + der;
            case TipoOperacion.resta:
                return izq - der;
            case TipoOperacion.multiplicacion:
                return izq * der;
            case TipoOperacion.division:
                // El entorno en la declaracion de variables en un "switch" es compartido.
                // Colocar llaves en cada "case" permite la creacion de entornos individuales.
                {
                    if (der === 0)
                    {
                        a.addError("Error: No es posible dividir por 0, " + this.loc)
                        return 0;
                    }
                    return izq / der;
                }
            case TipoOperacion.residuo:
                {
                    if (der === 0)
                    {
                        a.addError("Error: No es posible dividir por 0, " + this.loc)
                        return 0;
                    }
                    return izq % der;
                }
        }
    }

    izq: Expresion;
    der: Expresion;
    tipo: TipoOperacion;

    constructor(tipo: TipoOperacion, izq: Expresion, der: Expresion, fila: number, columna: number)
    {
        super(fila, columna);

        this.izq = izq;
        this.der = der;
        this.tipo = tipo;
    }
}