import { Arbol } from '../scope/Arbol';
import { Expresion } from '../AST/AST';
import { Simbolo } from '../Elementos/Simbolo';
import { TipoOperacion } from '../Elementos/TipoOperacion';

export class OperacionUnaria extends Expresion
{
    ejecutar(s: Map<string, Simbolo>, a: Arbol)
    {
        let unario = this.unario.ejecutar(s, a);

        switch (this.tipo)
        {
            case TipoOperacion.menos:
                return -unario;
        }
    }

    unario: Expresion;
    tipo: TipoOperacion;

    constructor(tipo: TipoOperacion, unario: Expresion, fila: number, columna: number)
    {
        super(fila, columna);

        this.unario = unario;
        this.tipo = tipo;
    }
}