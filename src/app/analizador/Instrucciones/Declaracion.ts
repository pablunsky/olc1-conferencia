import { Arbol } from '../scope/Arbol';
import { Expresion } from '../AST/AST';
import { Simbolo } from '../Elementos/Simbolo';

export class Declaracion extends Expresion
{
    ejecutar(s: Map<string, Simbolo>, a: Arbol)
    {
        let valor = this.valor.ejecutar(s, a);
        let simbolo = new Simbolo(this.id, valor);
        s.set(this.id, simbolo);
    }

    id: string;
    valor: Expresion;

    constructor(id: string, valor: Expresion, fila: number, columna: number)
    {
        super(fila, columna);
        this.id = id;
        this.valor = valor;
    }
}




