import { AccesoID } from '../Instrucciones/AccesoID';
import { Declaracion } from '../Instrucciones/Declaracion';
import { Expresion } from '../AST/AST';
import { OperacionBinaria } from '../Instrucciones/OperacionBinaria';
import { OperacionUnaria } from '../Instrucciones/OperacionUnaria';
import { Primitivo } from '../Elementos/Primitivo';
import { Print } from '../Elementos/Print';
import { Simbolo } from '../Elementos/Simbolo';
import { Utilidades } from '../Elementos/Utilidades';

export class Arbol
{

    u: Utilidades;
    errores = [];
    consola = [];
    instrucciones = [];

    constructor(ast: any)
    {

        this.u = new Utilidades();

        this.errores = [];
        this.instrucciones = [];

        ast.errores.forEach(element =>
        {
            this.errores.push(element);
        });

        ast.instrucciones.forEach(element =>
        {
            this.instrucciones.push(this.getExpresion(element));
        });

    }

    intepretar()
    {
        let simbolos = new Map<string, Simbolo>();

        this.instrucciones.forEach(element =>
        {
            element.ejecutar(simbolos, this);
        });
    }

    addError(error: string)
    {
        this.errores.push(error);
    }

    addConsola(msg: string)
    {
        this.consola.push(msg);
    }

    getExpresion(elemento: any): Expresion
    {
        switch (elemento.id)
        {
            case "Declaracion":
                return new Declaracion(elemento.identificador, this.getExpresion(elemento.valor), elemento.ln, elemento.col);
            case "Primitivo":
                return new Primitivo(this.u.getTipo(elemento.tipo), elemento.valor, elemento.ln, elemento.col);
            case "AccesoID":
                return new AccesoID(elemento.valor, elemento.ln, elemento.col);
            case "OperacionBinaria":
                return new OperacionBinaria(this.u.getTipoOperacion(elemento.tipo), this.getExpresion(elemento.izq), this.getExpresion(elemento.der), elemento.ln, elemento.col);
            case "OperacionUnaria":
                return new OperacionUnaria(this.u.getTipoOperacion(elemento.tipo), this.getExpresion(elemento.unario), elemento.ln, elemento.col);
            case "Print":
                return new Print(this.getExpresion(elemento.valor), elemento.ln, elemento.col);
        }
    }
}
