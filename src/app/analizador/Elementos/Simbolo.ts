import { TipoSimbolo } from './TipoSimbolo';

export class Simbolo
{
    id: string;
    valor: any;
    tipo: TipoSimbolo;

    constructor(id: string, valor: any)
    {
        this.id = id;
        this.valor = valor;
    }
}