import { TipoOperacion } from './TipoOperacion';
import { TipoSimbolo } from './TipoSimbolo';

export class Utilidades
{

    getTipo(tipo: string): TipoSimbolo
    {
        switch (tipo)
        {
            case 'entero':
                return TipoSimbolo.entero;
            case 'decimal':
                return TipoSimbolo.decimal;
        }
    }

    getTipoOperacion(tipo: string): TipoOperacion
    {
        switch (tipo)
        {
            case 'suma':
                return TipoOperacion.suma;
            case 'resta':
                return TipoOperacion.resta;
            case 'multiplicacion':
                return TipoOperacion.multiplicacion;
            case 'division':
                return TipoOperacion.division;
            case 'residuo':
                return TipoOperacion.residuo;
            case 'menos':
                return TipoOperacion.menos;
        }
    }

}