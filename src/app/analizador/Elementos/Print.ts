import { Arbol } from '../scope/Arbol';
import { Expresion } from '../AST/AST';
import { Simbolo } from '../Elementos/Simbolo';

export class Print extends Expresion
{
    ejecutar(s: Map<string, Simbolo>, a: Arbol)
    {
        let valor = this.valor.ejecutar(s, a);
        a.addConsola(valor.toString());
    }

    valor: Expresion;

    constructor(valor: Expresion, fila: number, columna: number)
    {
        super(fila, columna);
        this.valor = valor;
    }
}