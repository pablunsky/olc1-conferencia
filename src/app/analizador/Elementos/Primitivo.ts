import { Arbol } from '../Scope/Arbol';
import { Expresion } from '../AST/AST';
import { Simbolo } from './Simbolo';
import { TipoSimbolo } from './TipoSimbolo';

export class Primitivo extends Expresion
{
    ejecutar(s: Map<string, Simbolo>, a: Arbol)
    {
        return parseFloat(this.valor);
    }

    tipo: TipoSimbolo;
    valor: any;

    constructor(tipo: TipoSimbolo, valor: string, fila: number, columna: number)
    {
        super(fila, columna);

        this.tipo = tipo;
        this.valor = valor;
    }

}