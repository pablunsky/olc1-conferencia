export enum TipoOperacion
{

    suma = "suma",
    menos = "menos",
    resta = "resta",
    multiplicacion = "multiplicacion",
    division = "division",
    residuo = "residuo",

}