import { Arbol } from './analizador/scope/Arbol';
import { Injectable } from '@angular/core';
import { parserAPI } from './analizador/parser/parserAPI';

@Injectable({
  providedIn: 'root'
})
export class ParserService
{

  constructor() { }

  parseContent(content: string)
  {
    let ast = parserAPI.parse(content);
    let arbol = new Arbol(ast);
    arbol.intepretar();

    return { console: arbol.consola, errors: arbol.errores };
  }

}
