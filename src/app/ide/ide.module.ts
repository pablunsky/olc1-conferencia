import { CommonModule } from '@angular/common';
import { EditorModule } from '../editor/editor.module';
import { IdeComponent } from './ide.component';
import { MaterialModule } from '../material.module';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [IdeComponent],
  imports: [
    CommonModule,
    MaterialModule,
    EditorModule
  ],
  exports:
    [
      IdeComponent
    ]
})
export class IdeModule { }
