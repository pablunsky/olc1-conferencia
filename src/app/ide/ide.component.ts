import { Component, OnInit } from '@angular/core';

import { FormControl } from '@angular/forms';
import { Parser } from '@angular/compiler/src/ml_parser/parser';
import { ParserService } from '../parser.service';

@Component({
  selector: 'app-ide',
  templateUrl: './ide.component.html',
  styleUrls: ['./ide.component.css']
})
export class IdeComponent implements OnInit
{

  tabs = ['NuevoArchivo1.pablo'];
  selected = new FormControl(0);
  public editors = [];
  console: string;

  constructor(private parser: ParserService) { }

  ngOnInit(): void
  {
  }

  pushTab()
  {
    this.tabs.push(`NuevoArchivo${this.tabs.length + 1}.pablo`);
    this.selected.setValue(this.tabs.length - 1);
  }

  popTab()
  {
    if (this.tabs.length == 1)
    {
      return;
    }
    let index = this.selected.value;
    this.tabs.splice(index, 1);
  }

  runTab()
  {
    this.console = "";

    let content = this.editors[this.selected.value].getContent();
    let res = this.parser.parseContent(content);

    res.errors.forEach(element => 
    {
      this.console += element + "\n";
    })

    res.console.forEach(element => 
    {
      this.console += element + "\n";
    })

  }

}
